import json
import sys
import traceback
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

def getHeadlessBrowser():
  print('Initializing browser...')
  options = webdriver.ChromeOptions()
  options.add_argument('--headless')
  return webdriver.Chrome(options=options)


def loadStudents(path):
  try:
    allValid = True
    with open(path) as f:
      students = json.load(f)
    print('Checking ' + str(len(students)) + ' records.')
    for student in students:
      valid = True
      print('Checking ' + json.dumps(student))
      if not student.has_key('userName'):
        print('- Student is missing userName field')
        valid = False
      if not student.has_key('password'):
        print('- Student is missing password field')
        valid = False
      if not student.has_key('grade'):
        print('- Stundent is missing grade field')
        valid = False
      if student['grade'] > 8:
        if not student.has_key('firstName'):
          print('- Highschool student is missing firstName field')
          valid = False
        if not student.has_key('lastName'):
          print('- Highschool student is missing lastName field')
          valid = False
      if valid:
        print('OK')
      allValid = allValid and valid
    if valid:
      return students
    else:
      return None
  except Exception as e:
    print('Eception occured!', e)

def getYesRadioButton(browser):
  wait = WebDriverWait(browser, 30)
  return wait.until(EC.presence_of_element_located((By.XPATH, '(//div[contains(@class,"appsMaterialWizToggleRadiogroupElContainer")])[1]')))

def processHighSchoolStudent(student):
  browser = getHeadlessBrowser()
  print('Starting high school process...')
  browser.get('https://docs.google.com/forms/d/e/1FAIpQLSedNWLgRdQKVfNqT4gwYrq0PEJqj2vnOL5GHqfopjwnakC-0g/viewform')
  print('Logging in student...')
  email = browser.find_element_by_xpath('//input[@type="email"]')
  email.send_keys(student['userName'] + '@gapps.yrdsb.ca')
  email.send_keys(Keys.ENTER)
  wait = WebDriverWait(browser, 30)
  userName = wait.until(EC.presence_of_element_located((By.ID, 'UserName')))
  userName.send_keys(student['userName'])
  password = browser.find_element_by_id('Password')
  password.send_keys('d9s59ama')
  password.send_keys(Keys.ENTER)
  print('Loading form...')
  try:
    yes = getYesRadioButton(browser)
    print('Form loded, filling it in...')
  except:
    print('Probably on verify screen, clicking Continue.')
    continueButton = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(),"Continue")]')))
    continueButton.click()
    print('Loading form...')
  yes = getYesRadioButton(browser)
  print('Filling in form...')
  yes.click()
  firstName = browser.find_element_by_xpath('(//input[@type="text"])[1]')
  firstName.clear()
  firstName.send_keys(student['firstName'])
  lastName = browser.find_element_by_xpath('(//input[@type="text"])[2]')
  lastName.clear()
  lastName.send_keys(student['lastName'])
  submit = browser.find_element_by_xpath('(//span[contains(@class,"appsMaterialWizButtonPaperbuttonLabel")])[1]')
  print('Submitting form...')
  submit.click()

def processElementaryStudent(student):
  browser = getHeadlessBrowser()
  print('Loading questionnaire...')
  browser.get('https://covidscreening.yrdsb.ca/Account/Login?ReturnUrl=%2fHome%2fQuestionnaire')
  print('Filling in questionnaire...')
  userName = browser.find_element_by_id('UserName')
  userName.send_keys(student['userName'])
  password = browser.find_element_by_id('Password')
  password.send_keys(student['password'])
  password.send_keys(Keys.ENTER)
  yes = browser.find_element_by_xpath('(//label[@class="radioContainer"])[1]')
  yes.click()
  submit = browser.find_element_by_xpath('//input[@type="submit"]')
  print('Submitting questionnaire...')
  submit.click()

def main():
  args = sys.argv
  if len(args) != 2:
    print 'Command needs one and only one argument: the path to the student file'
    return
  students = loadStudents(args[1])
  if students is None:
    print('Fix errors in your input file')
  else:
    print('Confirming COVID-19 self check for ' + str(len(students)) + ' students...')
    for student in students:
      print('Processing ' + json.dumps(student))
      try:
        if student['grade'] > 8:
          processHighSchoolStudent(student)
        else:
          processElementaryStudent(student)
        print('Student ' + json.dumps(student) + ' is administered.')
      except Exception as e:
        print('Failed to confirm ' + json.dumps(student) + 'Exception: ', e)
        print(traceback.format_exc())
main()



