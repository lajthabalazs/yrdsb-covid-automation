# YRDSB Covid automation

This project aims at helping York Region parents get their kids to school healthy, by removing the administrative burden of the self check questionnaire, so they can focus on checking for symptoms instead of checking boxes.

For the self assessment, see https://www.york.ca/wps/wcm/connect/yorkpublic/9e5381ca-ec8b-4df1-8db5-42292f2a35fc/114_School+Screening+Tool_Sept3_2021.pdf?MOD=AJPERES&CVID=nKJnnH8

By following this guide you'll be able to set up a Raspberry Pi to fill in the school-required form acknowledging you performed the daily self assessment. It will send in the form every weekday at 6AM without any interaction needed. You - or your high school child - will receive the confirmation emails. The software is written in Python, and uses Selenium to automate the Chromium web browser, and fills in the form for you. As such, it's designed to run on headless systems, so it can be easily adopted to other platforms (Windows, OSX, cloud based virtual machines...) feel free to experiment.

## Prerequisits
- Raspberry Pi (tested on 4B, probably works on others)
- Raspbian
- Internet connection on the Raspberry
- Python 2.7 (should be installed with Raspbian)
- pip (should be installed with Raspbian)
- Chromium (should be installed with Raspbian)

## Setup

- Open a terminal, preferably in your home directory (/home/pi)
- Install Chrome driver for Chromium: `sudo apt-get install chromium-chromedriver`
- Install Selenium: `pip install selenium`
- Download the covid.py file from this repo: `wget -O covid.py https://gitlab.com/lajthabalazs/yrdsb-covid-automation/-/raw/main/covid.py?inline=false`
- Download the students.json file from this repo: `wget -O students.json https://gitlab.com/lajthabalazs/yrdsb-covid-automation/-/blob/main/students.json?inline=false`
- Edit and save the students.json file to fit your kids data: `nano students.json`

## Run the script

- Type the following in the terminal: `python covid.py students.json`
- Follow the logs in the terminal to see if every step goes smoothly (If you have a display connected to the Raspberry, you can comment out the line `options.add_argument('--headless')` with a preceding `#` and watch your script do all the work)

## Automate the script

- Make sure the clock on the Raspberry is set up properly! Fot timezone settings, check out [raspi-config](https://www.raspberrypi.org/documentation/computers/configuration.html)
- Type in the terminal: `crontab -e`
- At first use, it'll prompt you for a default editor, choose wisely
- Add a new line (cheat sheet to cron: https://devhints.io/cron): `0 6 * * 1,2,3,4,5 python /home/pi/covid.py /home/pi/students.json`

And you're ready! As long as your Pi has power at 6AM and it's connected to the Internet, you'll only have to worry about the assessment, masks, hand sanitizer...
